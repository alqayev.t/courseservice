package az.com.orient.course.persistance.service;

import az.com.orient.course.dto.dictionaries.DictionaryDto;
import az.com.orient.course.persistance.entity.Dictionary;
import az.com.orient.course.persistance.repository.DictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;

@Service
public class DictionaryServiceImpl implements DictionaryService {

    @Autowired
    private DictionaryRepository dictionaryRepository;

    @Override
    public List<DictionaryDto> findByKey(String key) {
        List<Dictionary> dictionaries = dictionaryRepository.findByDicKey(key);

        List<DictionaryDto> dictionaryDtos = dictionaries.stream()
                .map(d -> new DictionaryDto(d.getId(),d.getDicVal()))
                .collect(Collectors.toList());
        return dictionaryDtos;
    }

    @Override
    public Dictionary findById(Long id) {
        return dictionaryRepository.findById(id).get();
    }

    @Override
    public List<Dictionary> findAll() {
        return dictionaryRepository.findAll();
    }

    @Override
    public void remove(Long id) {
        dictionaryRepository.deleteById(id);
    }

    @Override
    public Dictionary save(Dictionary dictionary) {
        return dictionaryRepository.save(dictionary);
    }
}
