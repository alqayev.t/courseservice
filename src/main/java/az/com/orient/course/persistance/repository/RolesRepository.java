package az.com.orient.course.persistance.repository;

import az.com.orient.course.persistance.entity.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolesRepository extends JpaRepository<Roles,Long> {
}
