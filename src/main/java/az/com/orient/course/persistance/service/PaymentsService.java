package az.com.orient.course.persistance.service;

import az.com.orient.course.dto.editPayment.PaymentDto;
import az.com.orient.course.dto.search.SearchReqBean;
import az.com.orient.course.persistance.entity.Payments;

import java.util.List;

public interface PaymentsService {
    public PaymentDto findPaymentById(Long id);

    public List<PaymentDto> findAll();
    public List<PaymentDto> findAll(SearchReqBean reqBean);

    public void remove(Long id);

    public PaymentDto save(PaymentDto payment);

    public PaymentDto update(PaymentDto paymentDto);
}
