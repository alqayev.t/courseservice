package az.com.orient.course.persistance.repository;

import az.com.orient.course.persistance.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users,Long> {

    Users findByUsername(String username);
}
