package az.com.orient.course.persistance.service;

import az.com.orient.course.dto.Lessons.LessonsDto;
import az.com.orient.course.dto.editPayment.PaymentDto;
import az.com.orient.course.dto.search.SearchReqBean;
import az.com.orient.course.persistance.entity.Lessons;
import az.com.orient.course.persistance.entity.Payments;
import az.com.orient.course.persistance.entity.Users;
import az.com.orient.course.persistance.repository.PaymentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PaymentsServiceImpl implements PaymentsService {
    @Autowired
    private PaymentsRepository paymentsRepository;

    @Autowired
    private UsersService usersService;

    @Autowired
    private LessonsService lessonsService;

    @PersistenceContext
    private EntityManager em;

    @Override
    public PaymentDto findPaymentById(Long id) {
        Payments payments = paymentsRepository.findById(id).get();
        PaymentDto paymentDto = new PaymentDto().adapter(payments);
        return paymentDto;
    }

    @Override
    public List<PaymentDto> findAll() {
        List<Payments> allPayments = paymentsRepository.findAll();
        List<PaymentDto> paymentDtos = allPayments.stream().map(p -> new PaymentDto(
                p.getId(),
                p.getDate(),
                p.getSummary(),
                p.getBy().getId().toString(),
                p.getTo().getId().toString(),
                p.getLesson().getId().toString(),
                p.getStatus())).collect(Collectors.toList());
        return paymentDtos;
    }

    @Override
    public List<PaymentDto> findAll(SearchReqBean reqBean) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Payments> criteriaQuery = criteriaBuilder.createQuery(Payments.class);
        Root<Payments> paymentsRoot = criteriaQuery.from(Payments.class);

        Join<Payments, Users> usersByJoin = paymentsRoot.join("by", JoinType.INNER);
        Join<Payments, Users> usersToJoin = paymentsRoot.join("to", JoinType.INNER);
        Join<Payments, Lessons> lessonsToJoin = paymentsRoot.join("lesson", JoinType.INNER);

        List<Predicate> conditions = new ArrayList<>();

        if (Objects.nonNull(reqBean.getStatus())) {
            Predicate status = criteriaBuilder.equal(paymentsRoot.get("status"), reqBean.getStatus());
            conditions.add(status);
        }
        if (Objects.nonNull(reqBean.getById())) {
            Predicate byId = criteriaBuilder.equal(usersByJoin.get("id"), reqBean.getById());
            conditions.add(byId);
        }

        if (Objects.nonNull(reqBean.getToId())) {
            Predicate toId = criteriaBuilder.equal(usersToJoin.get("id"), reqBean.getToId());
            conditions.add(toId);
        }

        if (Objects.nonNull(reqBean.getLessonId())) {
            Predicate lessonId = criteriaBuilder.equal(lessonsToJoin.get("id"), reqBean.getLessonId());
            conditions.add(lessonId);
        }

        if (Objects.nonNull(reqBean.getSummary())) {
            Predicate summary = criteriaBuilder.equal(paymentsRoot.get("summary"), reqBean.getSummary());
            conditions.add(summary);
        }

        if (Objects.nonNull(reqBean.getStartPayDate())) {
            Predicate startDate = criteriaBuilder.greaterThan(paymentsRoot.get("date"), reqBean.getStartPayDate());
            conditions.add(startDate);
        }

        if (Objects.nonNull(reqBean.getEndPayDate())) {
            Predicate endDate = criteriaBuilder.lessThan(paymentsRoot.get("date"), reqBean.getEndPayDate());
            conditions.add(endDate);
        }
        Predicate status = criteriaBuilder.notEqual(paymentsRoot.get("status"), 0);
        conditions.add(status);

        criteriaQuery.where(conditions.toArray(new Predicate[conditions.size()]));

        List<Payments> payments = em.createQuery(criteriaQuery).getResultList();

        return payments.stream()
                .map(p -> new PaymentDto(p.getId(), p.getDate(),
                        p.getSummary(),
                        String.valueOf(p.getBy().getId()),
                        p.getBy().getFullName(),
                        String.valueOf(p.getTo().getId()),
                        p.getTo().getFullName(),
                        String.valueOf(p.getLesson().getId()),
                        p.getLesson().getName(),
                        p.getStatus()))
                .collect(Collectors.toList());
    }

    @Override
    public void remove(Long id) {
        paymentsRepository.deleteById(id);
    }

    @Override
    public PaymentDto save(PaymentDto p) {
        Users userById = usersService.findUserById(Long.valueOf(p.getUserById()));
        Users userToId = usersService.findUserById(Long.valueOf(p.getUserToId()));
        LessonsDto lessonById = lessonsService.findLessonById(Long.valueOf(p.getLessonId()));
        Lessons lessons = new Lessons().adapter(lessonById);
        Payments payments = new Payments(userById, userToId, lessons, p.getSummary(), p.getPayDate(), p.getStatus());
        payments = paymentsRepository.save(payments);

        p.adapter(payments);
        return p;
    }

    @Override
    public PaymentDto update(PaymentDto paymentDto) {
//            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
//            String dateInString = "07-01-2013";
//            Date date = formatter.parse(dateInString);
        Payments payments = paymentsRepository.updatePayments(Long.valueOf(paymentDto.getUserById()),
                Long.valueOf(paymentDto.getUserToId()),
                Long.valueOf(paymentDto.getLessonId()),
                paymentDto.getPayDate(),
                paymentDto.getSummary(),
                paymentDto.getStatus(),
                paymentDto.getId());
        paymentDto.adapter(payments);
        return paymentDto;
    }
}
