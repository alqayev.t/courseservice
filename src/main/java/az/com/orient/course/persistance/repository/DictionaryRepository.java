package az.com.orient.course.persistance.repository;

import az.com.orient.course.persistance.entity.Dictionary;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DictionaryRepository extends JpaRepository<Dictionary,Long> {

    public List<Dictionary> findByDicKey(String dicKey);
}
