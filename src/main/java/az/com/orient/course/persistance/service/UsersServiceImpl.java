package az.com.orient.course.persistance.service;

import az.com.orient.course.persistance.entity.Users;
import az.com.orient.course.persistance.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {
    @Autowired
    private UsersRepository repository;
    @Override
    public Users findUserById(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public List<Users> findAll() {
        return repository.findAll();
    }

    @Override
    public void remove(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Users save(Users users) {
        return repository.save(users);
    }
}
