package az.com.orient.course.persistance.service;


import az.com.orient.course.dto.dictionaries.DictionaryDto;
import az.com.orient.course.persistance.entity.Dictionary;

import java.util.List;

public interface DictionaryService {

    public Dictionary findById(Long id);

    public List<DictionaryDto> findByKey(String key);

    public List<Dictionary> findAll();

    public void remove(Long id);

    public Dictionary save(Dictionary dictionary);
}
