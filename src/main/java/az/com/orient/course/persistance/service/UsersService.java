package az.com.orient.course.persistance.service;

import az.com.orient.course.persistance.entity.Users;

import java.util.List;

public interface UsersService {

    public Users findUserById(Long id);

    public List<Users> findAll();

    public void remove(Long id);

    public Users save(Users users);

}
