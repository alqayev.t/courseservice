package az.com.orient.course.persistance.service;

import az.com.orient.course.dto.Lessons.LessonsDto;
import az.com.orient.course.persistance.entity.Lessons;

import java.util.List;

public interface LessonsService {
    public LessonsDto findLessonById(Long id);

    public List<LessonsDto> findAll();

    public void remove(Long id);

    public LessonsDto save(Lessons lessons);
}
