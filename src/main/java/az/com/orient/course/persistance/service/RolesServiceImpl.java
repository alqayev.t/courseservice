package az.com.orient.course.persistance.service;

import az.com.orient.course.persistance.entity.Roles;
import az.com.orient.course.persistance.repository.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolesServiceImpl  implements RolesService{
    @Autowired
    private RolesRepository repository;
    @Override
    public Roles findRoleById(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public List<Roles> findAll() {
        return repository.findAll();
    }

    @Override
    public void remove(Long id) {
            repository.deleteById(id);
    }

    @Override
    public Roles save(Roles roles) {
        return repository.save(roles);
    }
}
