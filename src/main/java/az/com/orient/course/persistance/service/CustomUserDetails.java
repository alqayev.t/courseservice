package az.com.orient.course.persistance.service;

import az.com.orient.course.dto.Response;
import az.com.orient.course.persistance.entity.Users;
import az.com.orient.course.persistance.repository.UsersRepository;
import az.com.orient.course.util.constans.ErrorCodes;
import az.com.orient.course.web.handling.exceptions.GlobalRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CustomUserDetails implements UserDetailsService {

    @Autowired
    private UsersRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Users user = repository.findByUsername(username);

        if (Objects.isNull(user)) {
            throw new GlobalRuntimeException(new Response(ErrorCodes.USER_NOT_FOUND));
        }

        List<GrantedAuthority> roles = user.getRoles()
                .stream()
                .map(r -> new SimpleGrantedAuthority(r.getName()))
                .collect(Collectors.toList());

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),roles);

        return userDetails;
    }
}
