package az.com.orient.course.persistance.repository;

import az.com.orient.course.persistance.entity.Lessons;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LessonsRepository extends JpaRepository<Lessons,Long> {
}
