package az.com.orient.course.persistance.service;

import az.com.orient.course.dto.Lessons.LessonsDto;
import az.com.orient.course.dto.editPayment.PaymentDto;
import az.com.orient.course.persistance.entity.Lessons;
import az.com.orient.course.persistance.repository.LessonsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LessonsServiceImpl implements LessonsService {
    
    @Autowired
    private LessonsRepository lessonsRepository;
    
    @Override
    public LessonsDto findLessonById(Long id) {
        Lessons lessons = lessonsRepository.findById(id).get();
        LessonsDto lessonsDto = new LessonsDto().adapter(lessons);
        return lessonsDto;
    }

    @Override
    public List<LessonsDto> findAll() {
        List<Lessons> all = lessonsRepository.findAll();
        List<LessonsDto> lessonsDtos = all.stream().map(p -> new LessonsDto(p.getId(),
                                                                            p.getName(),
                                                                            p.getStatus())).
                collect(Collectors.toList());
        return lessonsDtos;
    }

    @Override
    public void remove(Long id) {
        lessonsRepository.deleteById(id);

    }

    @Override
    public LessonsDto save(Lessons lessons) {
        Lessons save = lessonsRepository.save(lessons);
        LessonsDto lessonsDto = new LessonsDto().adapter(save);
        return lessonsDto;
    }
}
