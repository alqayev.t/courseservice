package az.com.orient.course.persistance.repository;

import az.com.orient.course.persistance.entity.Payments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.Date;

public interface PaymentsRepository extends JpaRepository<Payments,Long> {

    @Modifying
    @Query("update Payments u set u.by = ?1, u.to = ?2, u.lesson = ?3, u.date = ?4, u.summary = ?5, u.status = ?6 where u.id = ?7")
    public Payments updatePayments(Long by, Long to, Long lesson, Date date, BigDecimal summary, Integer status, Long id);
}
