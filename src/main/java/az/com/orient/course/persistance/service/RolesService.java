package az.com.orient.course.persistance.service;



import az.com.orient.course.persistance.entity.Roles;

import java.util.List;

public interface RolesService {
    public Roles findRoleById(Long id);

    public List<Roles> findAll();

    public void remove(Long id);

    public Roles save(Roles roles);
}
