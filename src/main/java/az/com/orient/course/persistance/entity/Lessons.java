package az.com.orient.course.persistance.entity;

import az.com.orient.course.dto.Lessons.LessonsDto;

import javax.persistence.*;

@Entity
@Table(name = "lessons")
public class Lessons {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer status = 1;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Lessons adapter (LessonsDto lessonsDto){
        Lessons lessons=new Lessons(lessonsDto.getId(),lessonsDto.getName(),lessonsDto.getStatus());
        return lessons;
    }

    public Lessons(Long id, String name, Integer status) {
        this.id=id;
        this.name = name;
        this.status = status;
    }

    public Lessons() {
    }
}
