package az.com.orient.course.persistance.entity;

import javax.persistence.*;

@Entity
@Table
public class Dictionary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "dic_key")
    private String dicKey;

    @Column(name = "dic_val")
    private String dicVal;

    private Integer status = 1;

    public Dictionary() {
    }

    public Dictionary(Long id, String dicKey, String dicVal, Integer status) {
        this.id = id;
        this.dicKey = dicKey;
        this.dicVal = dicVal;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDicKey() {
        return dicKey;
    }

    public void setDicKey(String dicKey) {
        this.dicKey = dicKey;
    }

    public String getDicVal() {
        return dicVal;
    }

    public void setDicVal(String dicVal) {
        this.dicVal = dicVal;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
