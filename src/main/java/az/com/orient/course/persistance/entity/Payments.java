package az.com.orient.course.persistance.entity;

import az.com.orient.course.dto.editPayment.PaymentDto;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "payments")
public class Payments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "by_id")
    private Users by;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "to_id")
    private Users to;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lesson_id")
    private Lessons lesson;

    private BigDecimal summary;

    private Date date;

    private Integer status = 1;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Users getBy() {
        return by;
    }

    public void setBy(Users by) {
        this.by = by;
    }

    public Users getTo() {
        return to;
    }

    public void setTo(Users to) {
        this.to = to;
    }

    public Lessons getLesson() {
        return lesson;
    }

    public void setLesson(Lessons lesson) {
        this.lesson = lesson;
    }

    public BigDecimal getSummary() {
        return summary;
    }

    public void setSummary(BigDecimal summary) {
        this.summary = summary;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }



    public Payments(Users by, Users to, Lessons lesson, BigDecimal summary, Date date, Integer status) {
        this.by = by;
        this.to = to;
        this.lesson = lesson;
        this.summary = summary;
        this.date = date;
        this.status = status;
    }

    public Payments() {
    }
}
