package az.com.orient.course.config;

import az.com.orient.course.util.JwtTokenUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class BeanConfig {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public JwtTokenUtil util() {
        return new JwtTokenUtil();
    }

    public static void main(String[] args) {
        BCryptPasswordEncoder e = new
                BCryptPasswordEncoder();
        System.out.println(e.encode("1"));
    }
}
