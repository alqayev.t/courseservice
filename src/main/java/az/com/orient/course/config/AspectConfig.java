package az.com.orient.course.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectConfig {

    @Pointcut("execution(   * az.com.orient.course.web.controller.*.*(..))")
    public void log () {}

    @Around("log()")

    public Object logExec (ProceedingJoinPoint point) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object object = point.proceed();
        long endTime = System.currentTimeMillis() - startTime;

        System.out.println("Method executed time : "+endTime);
        return object;
    }
}
