package az.com.orient.course.config;

import az.com.orient.course.dto.Response;
import az.com.orient.course.util.JwtTokenUtil;
import az.com.orient.course.util.constans.ErrorCodes;
import az.com.orient.course.web.handling.exceptions.GlobalRuntimeException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class AuthanticatinTokenFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    private Response responseBean = new Response();

    @Autowired
    private ObjectMapper objectMapper;

    private final String tokenHeader = "Authorization";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        final String requestHeader = request.getHeader(this.tokenHeader);

        String username = null;
        String authToken = null;

        System.out.println(request.getRequestURI()+"------");
        try {
            if ((requestHeader != null && requestHeader.startsWith("Bearer "))
                    || request.getRequestURI().endsWith("token")
                    || request.getRequestURI().endsWith("refreshToken")) {
                boolean hasRole = !request.getRequestURI().endsWith("token") && !request.getRequestURI().endsWith("refreshToken");
                if (hasRole) {
                    authToken = requestHeader.substring(7);
                    try {
                        username = jwtTokenUtil.getUsernameFromToken(authToken);
                        System.out.println("username : "+username);
                    } catch (MalformedJwtException e) {
                        logger.error("an error occured during getting username from token", e);
                        e.printStackTrace();
                        throw new GlobalRuntimeException(new Response(ErrorCodes.INWALID_JWT));
                    } catch (ExpiredJwtException e) {
                        e.printStackTrace();
                        logger.warn("the token is expired and not valid anymore", e);
                        throw new GlobalRuntimeException(new Response(ErrorCodes.EXPIRED_JWT));
                    }
                }
                if (username!=null) {
                    UserDetails userDetails = userDetailsService.loadUserByUsername(username);

                    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                            userDetails,null,userDetails.getAuthorities()
                    );
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                }
                doFilter(request,response,chain);
            } else {
                throw new GlobalRuntimeException(new Response(ErrorCodes.TOKE_IS_EMPTY));
            }
        } catch (RuntimeException ex) {

            if (ex instanceof GlobalRuntimeException) {
                GlobalRuntimeException glob = (GlobalRuntimeException)ex;
                this.responseBean = ((GlobalRuntimeException) ex).getResponse();
            }else {
                this.responseBean = new Response(ErrorCodes.GENERAL_ERROR);
            }

            //TODO
            ex.printStackTrace();
            response.setContentType("application/json");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.getWriter().write(convertObjectToJson(this.responseBean));
        }
    }
    /*create response json*/
    public String convertObjectToJson(Object object) throws JsonProcessingException {
        Optional<Object> opt = Optional.ofNullable(object);
        if (!opt.isPresent()) return null;
        return objectMapper.writeValueAsString(object);
    }
}
