package az.com.orient.course.web.handling;

import az.com.orient.course.dto.Response;
import az.com.orient.course.util.constans.ErrorCodes;
import az.com.orient.course.web.handling.exceptions.GlobalRuntimeException;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class PushBack {

    @ExceptionHandler(GlobalRuntimeException.class)
    @ResponseBody
    public Response globalException(GlobalRuntimeException ex) {
        return ex.getResponse();
    }

    @ExceptionHandler(ExpiredJwtException.class)
    @ResponseBody
    public Response globalException() {
        return new Response(ErrorCodes.EXPIRED_JWT);
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseBody
    public Response forbidden() {
        return new Response(ErrorCodes.FORBIDDEN);
    }
}
