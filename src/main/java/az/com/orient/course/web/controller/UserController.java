package az.com.orient.course.web.controller;


import az.com.orient.course.dto.Response;
import az.com.orient.course.persistance.entity.Users;
import az.com.orient.course.persistance.service.UsersService;
import az.com.orient.course.util.constans.ErrorCodes;
import az.com.orient.course.web.handling.exceptions.GlobalRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping("/user")
/**
 * /register- Altuna
 * /findOne- Altuna
 * /update-Shahriyar
 * /delete +
 * /users- Shahriyar
 */
public class UserController {

    @Autowired
    private UsersService usersService;

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Response removeUser(@PathVariable ("id") Long id) {
        Users user = usersService.findUserById(id);

        if (Objects.isNull(user)) {
            throw new GlobalRuntimeException(new Response(ErrorCodes.USER_NOT_FOUND));
        }
        user.setStatus(0);
        usersService.save(user);
        return new Response(ErrorCodes.SUCCESS);
    }
}
