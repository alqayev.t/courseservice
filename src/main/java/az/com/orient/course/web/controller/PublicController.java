package az.com.orient.course.web.controller;

import az.com.orient.course.dto.Lessons.LessonsDto;
import az.com.orient.course.dto.Lessons.LessonsResp;
import az.com.orient.course.dto.Response;
import az.com.orient.course.dto.dictionaries.DictionaryDto;
import az.com.orient.course.dto.dictionaries.DictionaryResp;
import az.com.orient.course.dto.editPayment.PaymentResp;
import az.com.orient.course.dto.editPayment.PaymentDto;
import az.com.orient.course.dto.search.SearchReqBean;
import az.com.orient.course.persistance.service.DictionaryService;
import az.com.orient.course.persistance.service.LessonsService;
import az.com.orient.course.persistance.service.PaymentsService;
import az.com.orient.course.util.constans.ErrorCodes;
import az.com.orient.course.web.handling.exceptions.GlobalRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/all")
/**
 *  /lessons +
 *  /paid payment +
 *  /payments +
 *  /findOnePayment +
 *  /dictionaries (key) +
 *  /advanceSearch +
 *  /updatePayment +
 *  /changePaymentStatus - id ve status gelir, update, status ,1 gozleme,2 - odenildi,3 - reject
 *  /grant - Altuna silinme
 */
public class PublicController {


    @Autowired
    private LessonsService lessonsService;
    
    @Autowired
    private DictionaryService dictionaryService;

    @Autowired
    private PaymentsService paymentsService;

    @GetMapping("/dictionaries/{key}")
    public DictionaryResp dictionaries(@PathVariable("key") String key) {
        List<DictionaryDto> dictionaryDtos = dictionaryService.findByKey(key);
        DictionaryResp response = new DictionaryResp(ErrorCodes.SUCCESS,dictionaryDtos);

        return response;
    }


    @GetMapping("/lessons")
    public LessonsResp getLesson(){
        List<LessonsDto> allLessons = lessonsService.findAll();
        LessonsResp resp = new LessonsResp(ErrorCodes.SUCCESS,allLessons);
        return resp;
    }

    @PostMapping("/payment")
    public PaymentResp setPayment(@RequestBody PaymentDto paymentDto){
        PaymentDto savePayments = paymentsService.save(paymentDto);
        PaymentResp paymentResp = new PaymentResp(ErrorCodes.SUCCESS,savePayments);
        return paymentResp;
    }

    @GetMapping("/payment")
    public PaymentResp getPayment(){
        List<PaymentDto> allPayments = paymentsService.findAll();
        PaymentResp paymentResp = new PaymentResp(ErrorCodes.SUCCESS,allPayments);
        return paymentResp;
    }

    @PostMapping("/payments")
    public PaymentResp payments(@RequestBody SearchReqBean input) {

        List<PaymentDto> allPayments = paymentsService.findAll(input);
        PaymentResp paymentResp = new PaymentResp(ErrorCodes.SUCCESS,allPayments);

        return paymentResp;
    }

    @GetMapping("/payment/{id}")
    public PaymentResp getPayment(@PathVariable("id") Long id){
        PaymentDto paymentById = paymentsService.findPaymentById(id);
        if (Objects.isNull(paymentById)) {
            throw new GlobalRuntimeException(new Response(ErrorCodes.PAYMENT_NOT_FOUND));
        }
        PaymentResp paymentResp = new PaymentResp(ErrorCodes.SUCCESS,paymentById);
        return  paymentResp;
    }

    @PutMapping("/payment")
    public PaymentResp updatePayment(@RequestBody PaymentDto paymentDto){
        PaymentDto updatePayment = paymentsService.update(paymentDto);
        PaymentResp paymentResp = new PaymentResp(ErrorCodes.SUCCESS,updatePayment);
        return  paymentResp;
    }
}
