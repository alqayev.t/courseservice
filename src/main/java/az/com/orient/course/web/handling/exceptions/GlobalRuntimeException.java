package az.com.orient.course.web.handling.exceptions;

import az.com.orient.course.dto.Response;
import az.com.orient.course.util.constans.ErrorCodes;

public class GlobalRuntimeException extends RuntimeException {

    private Response response;

    public GlobalRuntimeException(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }
}
