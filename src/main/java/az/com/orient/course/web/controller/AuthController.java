package az.com.orient.course.web.controller;

import az.com.orient.course.dto.Response;
import az.com.orient.course.dto.token.TokenReqBean;
import az.com.orient.course.dto.token.TokenRespBean;
import az.com.orient.course.util.JwtTokenUtil;
import az.com.orient.course.util.constans.ErrorCodes;
import az.com.orient.course.web.handling.exceptions.GlobalRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
/**
 * /token +
 * /refreshToken +
 */
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;


    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping("/token")
    @ResponseBody
    public TokenRespBean token(@RequestBody TokenReqBean request){

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(request.getUsername(),request.getPassword());

        try {
            Authentication authentication = authenticationManager.authenticate(authenticationToken);
        }catch (BadCredentialsException e) {
            throw new GlobalRuntimeException(new Response(ErrorCodes.USER_NOT_FOUND));
        }

        String token = jwtTokenUtil.generateToken(request.getUsername());
        TokenRespBean response = new TokenRespBean(ErrorCodes.SUCCESS,token);
        return response;
    }

    @PostMapping("/refreshToken")
    @ResponseBody
    public TokenRespBean token(@RequestHeader("Authorization")String token){
        token = jwtTokenUtil.refreshToken(token.substring(7));
        TokenRespBean response = new TokenRespBean(ErrorCodes.SUCCESS,token);
        return response;
    }
}
