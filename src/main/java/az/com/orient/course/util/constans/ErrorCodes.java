package az.com.orient.course.util.constans;

public enum ErrorCodes {

    GENERAL_ERROR(5001,"General Error"),
    PAYMENT_NOT_FOUND(5002,"Payment not found"),
    INWALID_JWT(5003,"InvalidJWT"),
    EXPIRED_JWT(5004,"JWT expired"),
    USER_NOT_FOUND(5005,"User not found"),
    TOKE_IS_EMPTY(5006,"Token is empty"),
    FORBIDDEN(5007,"Permission denied"),
    SUCCESS(0,"Success");


    private int code;

    private String message;

    ErrorCodes(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
