package az.com.orient.course.dto.Lessons;

import az.com.orient.course.dto.Response;
import az.com.orient.course.util.constans.ErrorCodes;

import java.util.List;

public class LessonsResp extends Response {

    private LessonsDto lessonsDto;

    private  List<LessonsDto> lessonsDtoList;

    public LessonsResp() {
    }

    public LessonsResp(ErrorCodes errorCodes, LessonsDto lessonsDto) {
        super(errorCodes);
        this.lessonsDto = lessonsDto;
    }

    public LessonsResp(ErrorCodes errorCodes, List<LessonsDto> allLesson) {
        super(errorCodes);
        this.lessonsDtoList = allLesson;
    }

    public LessonsDto getLessonsDto() {
        return lessonsDto;
    }

    public void setLessonsDto(LessonsDto lessonsDto) {
        this.lessonsDto = lessonsDto;
    }
}
