package az.com.orient.course.dto.search;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

public class SearchReqBean {

    @Override
    public String toString() {
        return "SearchReqBean{" +
                "startPayDate=" + startPayDate +
                ", endPayDate=" + endPayDate +
                ", summary=" + summary +
                ", byId=" + byId +
                ", toId=" + toId +
                ", lessonId=" + lessonId +
                ", status=" + status +
                '}';
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startPayDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endPayDate;

    private BigDecimal summary;

    private Long byId;

    private Long toId;

    private Long lessonId;

    private Integer status;

    public Date getStartPayDate() {
        return startPayDate;
    }

    public void setStartPayDate(Date startPayDate) {
        this.startPayDate = startPayDate;
    }

    public Date getEndPayDate() {
        return endPayDate;
    }

    public void setEndPayDate(Date endPayDate) {
        this.endPayDate = endPayDate;
    }

    public BigDecimal getSummary() {
        return summary;
    }

    public void setSummary(BigDecimal summary) {
        this.summary = summary;
    }

    public Long getById() {
        return byId;
    }

    public void setById(Long byId) {
        this.byId = byId;
    }

    public Long getToId() {
        return toId;
    }

    public void setToId(Long toId) {
        this.toId = toId;
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
