package az.com.orient.course.dto.dictionaries;

import az.com.orient.course.dto.Response;
import az.com.orient.course.util.constans.ErrorCodes;

import java.util.List;

public class DictionaryResp extends Response {

    public DictionaryResp(ErrorCodes errorCodes, List<DictionaryDto> dictionaries) {
        super(errorCodes);
        this.dictionaries = dictionaries;
    }
    public DictionaryResp() {

    }

    private List<DictionaryDto> dictionaries;

    public List<DictionaryDto> getDictionaries() {
        return dictionaries;
    }

    public void setDictionaries(List<DictionaryDto> dictionaries) {
        this.dictionaries = dictionaries;
    }
}
