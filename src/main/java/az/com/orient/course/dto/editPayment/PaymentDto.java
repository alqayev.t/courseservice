package az.com.orient.course.dto.editPayment;

import az.com.orient.course.persistance.entity.Payments;

import java.math.BigDecimal;
import java.util.Date;

public class PaymentDto {

    private Long id;

    private Date payDate;

    private BigDecimal summary;

    private String userById;

    private String fullByName;

    private String userToId;

    private String fullToName;

    private String lessonId;

    private String lessonName;

    private Integer status;

    public PaymentDto() {
    }

    public PaymentDto(Long id,
                      Date payDate,
                      BigDecimal summary,
                      String userById,
                      String fullByName,
                      String userToId,
                      String fullToName,
                      String lessonId,
                      String lessonName,
                      Integer status) {
        this.id = id;
        this.payDate = payDate;
        this.summary = summary;
        this.userById = userById;
        this.fullByName = fullByName;
        this.userToId = userToId;
        this.fullToName = fullToName;
        this.lessonId = lessonId;
        this.lessonName = lessonName;
        this.status = status;
    }

    public PaymentDto adapter(Payments p){
        PaymentDto paymentDto = new PaymentDto(p.getId(),p.getDate(),p.getSummary(),p.getBy().getFullName(),p.getTo().getFullName(),p.getLesson().getName(),p.getStatus());
        return paymentDto;
    }

    public PaymentDto(Long id, Date payDate, BigDecimal summary, String userById, String userToId, String lessonId, Integer status) {
        this.id = id;
        this.payDate = payDate;
        this.summary = summary;
        this.userById = userById;
        this.userToId = userToId;
        this.lessonId = lessonId;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public BigDecimal getSummary() {
        return summary;
    }

    public void setSummary(BigDecimal summary) {
        this.summary = summary;
    }

    public String getUserById() {
        return userById;
    }

    public void setUserById(String userById) {
        this.userById = userById;
    }

    public String getUserToId() {
        return userToId;
    }

    public void setUserToId(String userToId) {
        this.userToId = userToId;
    }

    public String getLessonId() {
        return lessonId;
    }

    public void setLessonId(String lessonId) {
        this.lessonId = lessonId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
