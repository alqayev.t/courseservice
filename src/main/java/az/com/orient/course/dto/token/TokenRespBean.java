package az.com.orient.course.dto.token;

import az.com.orient.course.dto.Response;
import az.com.orient.course.dto.editPayment.PaymentDto;
import az.com.orient.course.util.constans.ErrorCodes;

public class TokenRespBean extends Response {

    public TokenRespBean(ErrorCodes errorCodes, String token) {
        super(errorCodes);
        this.token = token;
    }

    public TokenRespBean() {
    }
    public TokenRespBean(String token) {
        this.token=token;
    }

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
