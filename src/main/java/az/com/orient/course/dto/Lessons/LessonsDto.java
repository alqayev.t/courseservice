package az.com.orient.course.dto.Lessons;

import az.com.orient.course.persistance.entity.Lessons;

public class LessonsDto {
    private Long id;

    private String name;

    private Integer status = 1;

    public LessonsDto(Long id, String name, Integer status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public LessonsDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LessonsDto adapter(Lessons l) {
        LessonsDto lessonsDto = new LessonsDto(l.getId(),l.getName(),l.getStatus());
        return lessonsDto;
    }
}
