package az.com.orient.course.dto.dictionaries;

public class DictionaryDto {

    private Long id;

    private String dicVal;

    public DictionaryDto() {
    }

    public DictionaryDto(Long id, String dicVal) {
        this.id = id;
        this.dicVal = dicVal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDicVal() {
        return dicVal;
    }

    public void setDicVal(String dicVal) {
        this.dicVal = dicVal;
    }
}
