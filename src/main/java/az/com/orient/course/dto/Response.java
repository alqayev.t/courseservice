package az.com.orient.course.dto;

import az.com.orient.course.util.constans.ErrorCodes;

public class Response {

    private Integer code;

    private String message;

    public Response() {
    }
    public Response(ErrorCodes errorCodes) {
        code = errorCodes.getCode();
        message = errorCodes.getMessage();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
