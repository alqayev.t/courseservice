package az.com.orient.course.dto.editPayment;

import az.com.orient.course.dto.Response;
import az.com.orient.course.util.constans.ErrorCodes;

import java.util.List;

public class PaymentResp extends Response {

    private PaymentDto paymentInfo;

    private List<PaymentDto> paymentDtos;

    public PaymentResp(ErrorCodes errorCodes, PaymentDto paymentInfo) {
        super(errorCodes);
        this.paymentInfo = paymentInfo;
    }
    public PaymentResp() {
    }

    public PaymentResp(ErrorCodes errorCodes, List<PaymentDto> allPayments) {
        super(errorCodes);
        this.paymentDtos = allPayments;
    }

    public PaymentDto getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentDto paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public List<PaymentDto> getPaymentDtos() {
        return paymentDtos;
    }

    public void setPaymentDtos(List<PaymentDto> paymentDtos) {
        this.paymentDtos = paymentDtos;
    }
}
